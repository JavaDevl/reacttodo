import React from 'react'
import { render } from 'react-dom'
import {Router, Route, IndexRoute, hashHistory} from 'react-router';
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import todoApp from './reducers'
import App from './components/App'

let jsonTodo = {
    items: [
    {
        "tags": ["done", "step0", "today"],
        "state": "done",
        "text": "Start project",
    },
    {
        "due": "2016-01-01T00:00:00",
        "state": "active",
        "tags": ["step1", "today"],
        "text": "Set up environment",
        "tz": "Europe/Zurich"
    },
    {
        "due": "2016-10-01T01:00:00",
        "state": "active",
        "tags": ["step2"],
        "text": "Setup expressJS for static",
        "tz": "Europe/Zurich"
    },
    {
        "due": "2016-10-02T00:00:00",
        "state": "active",
        "tags": ["step3"],
        "text": "Build a storage with redux",
        "tz": "America/Los_Angeles"
    },
    {
        "due": "2016-10-03T00:00:00",
        "state": "active",
        "tags": ["passive", "step3", "today"],
        "text": "Add actions to the app",
        "tz": "Europe/Warsaw"
    },
    {
        "due": "2016-10-04T02:00:00",
        "state": "active",
        "tags": ["step4"],
        "text": "Add components",
        "tz": "America/New_York"
    },
    {
        "due": "2016-10-05T03:00:00",
        "state": "active",
        "tags": ["step6", "optional"],
        "text": "Write tests",
        "tz": "America/New_York"
    },
    {


        "due": "2016-10-05T04:00:00",
        "state": "active",
        "tags": ["step6", "important"],
        "text": "Commit to the .git of choice",
        "tz": "America/New_York"
    },
    {
        "tags": ["step6", "important"],
        "state": "active",
        "text": "Finish project"
    }
]};

let store = createStore(todoApp, {todos:jsonTodo})

render(
<Provider store={store}>
    <Router history={hashHistory}>
        <Route path="/" component={App}>
        </Route>
    </Router>
    </Provider>,
    document.getElementById('root')
)