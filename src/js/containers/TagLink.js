import React from 'react'
import { connect } from 'react-redux'
import { deleteTodo } from '../actions'
import { setVisibilityFilter } from '../actions'

let Tags = ({ tags, key, dispatch }) => {
    return (
    <div>
        {tags.map(tag => (
                <span key={key} onClick={
                        e=>{
                        var TagFilter=e.target.innerHTML;
                        dispatch(setVisibilityFilter('SHOW_BY_TAGS', TagFilter))}}
                      class='tags'>
                    {tag}</span>
            ))
        }
    </div>
)};

Tags = connect()(Tags)

export default Tags;
