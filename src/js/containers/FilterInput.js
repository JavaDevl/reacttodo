import React from 'react'
import { connect } from 'react-redux'
import { setVisibilityFilter } from '../actions'

let FilterInput = ({dispatch}) => {
    let filterInput;
    return (
    <input ref={node => {filterInput = node}}
           onKeyUp={()=>{
                var filterInputText = filterInput.value;
                var showFilter = 'SHOW_ALL';

                if(filterInputText.length>=2)
                    showFilter = 'SHOW_BY_FILTER';

                dispatch(setVisibilityFilter(showFilter, filterInputText));

           }} />
    )
};

FilterInput = connect()(FilterInput)

export default FilterInput;
