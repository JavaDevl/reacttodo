import React from 'react'
import {connect} from 'react-redux'
import {addTodo} from '../actions'
import DeleteTodo  from './DeleteTodo'
import UndoTodo  from './UndoTodo'
import FilterInput  from './FilterInput'

let AddTodo = ({dispatch}) => {
    let inputTodo,
        inputTag,
        inputTz;

    return (
        <div>
            <form onSubmit={e => {
        e.preventDefault()
        if (!inputTodo.value.trim()) return;

        dispatch(addTodo(inputTodo.value, inputTag.value.split(",")))
        inputTodo.value = '';
        inputTag.value = '';
      }}>
                {'Todo: '}<input ref={ node => {inputTodo = node} }/><br/>
                {'Tags: '}<input ref={ node => {inputTag = node} }/> {'(separated with a comma)'}<br />
                                <button type="submit">
                                    Add Todo
                                </button>
                                <DeleteTodo />
                                <UndoTodo /><br />
                                <FilterInput />
            </form>
        </div>
    )
}
AddTodo = connect()(AddTodo)

export default AddTodo