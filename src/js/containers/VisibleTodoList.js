import { connect } from 'react-redux'
import { toggleTodo } from '../actions'
import TodoList from '../components/TodoList'

const getVisibleTodos = (todos, filter) => {
    switch (filter.section) {
        case 'SHOW_ALL':
            return todos.items
        case 'SHOW_COMPLETED':
            return todos.items.filter(todo => todo.state==='done')
        case 'SHOW_ACTIVE':
            return todos.items.filter(todo => todo.state==='active')
        case 'SHOW_BY_TAGS':
            return todos.items.filter(todo => todo.tags.indexOf(filter.wordFilter)>-1)
        case 'SHOW_BY_FILTER':
           return todos.items.filter(todo => todo.text.toLowerCase().indexOf(filter.wordFilter.toLowerCase())>-1)
    }
}       

const mapStateToProps = (state) => {
    return {
        todos: getVisibleTodos(state.todos, state.visibilityFilter)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onTodoClick: (id) => {
            dispatch(toggleTodo(id))
        }
    }
}

const VisibleTodoList = connect(
    mapStateToProps,
    mapDispatchToProps
)(TodoList)

export default VisibleTodoList