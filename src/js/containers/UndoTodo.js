import React from 'react'
import { connect } from 'react-redux'
import { undoTodo } from '../actions'

let UndoTodo = ({ dispatch }) => {
    return (
        <button type="button" 
                onClick={()=>{
                    dispatch(undoTodo());
                    }}
                >
            Undo Todo
        </button>
    )
}

UndoTodo = connect()(UndoTodo)

export default UndoTodo