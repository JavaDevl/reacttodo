import React from 'react'
import { connect } from 'react-redux'
import { deleteTodo } from '../actions'

let DeleteTodo = ({ dispatch }) => {
    return (
                <button onClick={()=>{
                    let todoIds = [];
                    let checkTodos=document.getElementsByClassName("check-todo");
                    let checkTodosArr = Object.keys(checkTodos);

                    checkTodosArr.forEach( key => {
                        if (checkTodos[key].checked)
                            todoIds.push(key);
                    });
                    
                    dispatch(deleteTodo(todoIds));
                    checkTodosArr.forEach( key => {
                        checkTodos[key].checked=false;
                    });
                    }
                } type="button">
                    Delete Todo
                </button>
    )
}
DeleteTodo = connect()(DeleteTodo)

export default DeleteTodo