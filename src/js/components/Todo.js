import React, { PropTypes } from 'react'
import Tags from '../containers/TagLink'

let leadWithZero = (t) =>{
    return ('0'+ t).slice(-2)
}
let ISOdateToHumanReadable = (date) => {
let d = new Date(date);
    return leadWithZero(d.getDay()) + "/" + leadWithZero(d.getMonth()) + "/" + d.getFullYear() + " " + leadWithZero(d.getHours()) + ":" + leadWithZero(d.getMinutes());
};

let Todo = ({ onClick, state, text, tags, keyTodo, tz, due }) =>(
    <div>
        <li
            key={keyTodo}
            onClick={onClick}
            style={{
      textDecoration: (state==='done') ? 'line-through' : 'none'
    }}
        >
            <input type='checkbox' class="check-todo" value={keyTodo}/>
            {text}
            { due? (<span class="dateTimeFormat">{'[ ' + ISOdateToHumanReadable(due)} {'(' + tz + ') ]'}</span>) :null}

        </li>
        <Tags tags={tags}/>
    </div>
);


export default Todo