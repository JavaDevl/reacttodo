import React, { PropTypes } from 'react'
import Todo from './Todo'

const TodoList = ({ todos, onTodoClick }) => (
    <ul>
        {todos.map((todo, todoKey) =>{
                return <Todo
                    keyTodo={todoKey}
                    {...todo}
                    onClick={() => onTodoClick(todoKey)}
                />}
        )}
    </ul>
)

export default TodoList