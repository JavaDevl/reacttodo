let nextTodoId = 0
export const addTodo = (text, tags, tz) => {
    return {
        type: 'ADD_TODO',
        tags,
        text
    }
}

export const deleteTodo = (ids) => {
    return {
        type: 'DELETE_TODO',
        ids
    }
}

export const undoTodo = (ids) => {
    return {
        type: 'UNDO_TODO'
    }
}

export const setVisibilityFilter = (filter, wordFilter="") => {
    return {
        type: 'SET_VISIBILITY_FILTER',
        filter,
        wordFilter
    }
}
export const toggleTodo = (id) => {
    return {
        type: 'TOGGLE_TODO',
        id
    }
}