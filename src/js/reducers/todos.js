import jstz from 'jstz';
const timezoneName = jstz.determine().name();

const todo = (state = {}, action) => {
    switch (action.type) {
        case 'ADD_TODO':
            return {
                text: action.text,
                state: 'active',
                tags: action.tags,
                due: (new Date()).toISOString().slice(0,-5),
                tz: timezoneName
            }
        
        case 'TOGGLE_TODO':
            return Object.assign(state, state[action.id], {
                state: state.state === 'done'? 'active': 'done'
            })
        
        case 'DELETE_TODO':
            return [
                ...state.slice(0, action.id),
                ...state.slice(action.id+1)
            ];
        
        default:
            return state
    }
}

const todos = (state = [], action) => {
    console.log(state);
    let items = state.items;
    state.past = state.past || [];

    switch (action.type) {
        case 'ADD_TODO':
            return {
                ...state,
                items:[
                    ...items,
                    todo(undefined, action)
                ],
                past: [...state.past, state]
            }
        case 'TOGGLE_TODO':
            return {
                ...state,
               items: [
                    ...items.slice(0, action.id),
                   {...items[action.id],
                       state: items[action.id].state === 'done'? 'active': 'done'
                   },
                    ...items.slice(action.id+1)
                ],
                past: [...state.past, state]
            };

        case 'DELETE_TODO':
            let ids = action.ids;
            var clone = items.slice();
            ids.map( id => {
                clone.splice(id,1)
            });
            return {
                ...state,
                items: clone,
                past: [...state.past, state]
            }

        case 'UNDO_TODO':
            let prevState = state.past[state.past.length -1];
            state.past.pop();
            return prevState;
        default:
            return state
    }
}

export default todos