const visibilityFilter = (state, action) => {
    switch (action.type) {
        case 'SET_VISIBILITY_FILTER':
            return Object.assign({}, {section:action.filter, wordFilter: action.wordFilter})
        default:
            return Object.assign({}, {section:'SHOW_ALL', wordFilter: ''})
    }
}

export default visibilityFilter